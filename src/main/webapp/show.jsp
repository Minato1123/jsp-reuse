<%-- 
    Document   : show
    Created on : 2022年10月25日, 下午3:34:49
    Author     : carrie
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%-- 可以顯示中文（解碼） --%>
<%request.setCharacterEncoding("UTF-8");%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            p {
                border: 0.1rem solid rgba(0, 0, 0, 0.2);
                width: 14rem;
                padding: 0.5rem;
            }
        </style>
    </head>
    <body>
        <%@include file="/WEB-INF/DateNav.jsp" %>
        <%
            String date = request.getParameter("date");
            String content = request.getParameter("content"); // 直接這樣寫的話會有危險，使用者可以輸入程式碼去取得源碼內容，甚至改寫
            content = content.replace("<", "&lt;"); // 將 <> 做替換，如果不回傳替換，不會動到原始字串
            content = content.replace(">", "&gt;");
            
            session.setAttribute("date", date);
            session.setAttribute("content", content);
        %>
        <h2>Date: <%= date %></h2>
        <p><%= content %></p>
    </body>
</html>
