<%-- 
    Document   : newjsp
    Created on : 2022年11月1日, 下午2:25:28
    Author     : carrie
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <!-- 轉址，轉址的檔案不能有其他輸出 -->
        <%--
            <jsp:forward page="index.jsp" /> 

            // 方法一：不能轉去專案列以外其他網站（網址列不會變） 
            
        --%>
        
        <%
            // response.sendRedirect("index.jsp"); 

            // 方法二：可以轉去外部網站（重新連線）
            // 定義在 HttpServletResponse
            // 效率較低（因為cilent會再 request 一次）
            // 適用於跳至外部網站或回主畫面使用
        %>
        
        <!-- 輸入網址，提交後轉址 -->
        <form method="post" action="redirectServlet">
            URL: <input name="url" /><input type="submit" value="submit">
        </from>
    </body>
</html>
