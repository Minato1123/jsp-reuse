<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            form {
                width: 14rem;
                display: flex;
                flex-direction: column;
                justify-content: center;
            }
            
            textarea {
                margin-bottom: 0.5rem;
            }
        </style>
    </head>
    <body>
        <%@include file="/WEB-INF/DateNav.jsp" %>
        <%
            String content = (String) session.getAttribute("content");
            // 這樣的型別轉換 null 不會被轉成 "null"，依然會是 null
            if (content == null) {
                content = "";
            };
        %>
        <form method="post" action="show.jsp"> <!-- action 是相對路徑 -->
            <p>Date: <input type="date" name="date" value="<%= session.getAttribute("date") %>" /></p>
            <textarea name="content" rows="6"><%= content %></textarea>
            <input type="submit" value="Submit">
        </form>
    </body>
</html>
