<%-- 
    Document   : UseInclude
    Created on : Sep 23, 2018, 9:07:19 PM
    Author     : lendle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            * {
                margin: 0;
            }
            
            .content {
                display: flex;
                justify-content: center;
            }
            
            .footer-block {
                position: absolute;
                bottom: 1rem;
            }
        </style>
    </head>
    <body>
        <div class="content">
            <h1>Hello World!</h1>
        </div>
        <div class="footer-block">
            <!--
            利用 jsp:include 來加入 /WEB-INF/ContactSection.jsp
            （不是 directive）
            -->
            <jsp:include page="/WEB-INF/ContactSection.jsp" /> 
            <!-- 會產生出：org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "/WEB-INF/ContactSection.jsp", out, false); -->
            <!-- 在 run time 的時候才會抓 -->
        </div>
    </body>
</html>
