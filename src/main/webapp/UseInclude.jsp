<%-- 
    Document   : UseInclude
    Created on : Sep 23, 2018, 9:07:19 PM
    Author     : lendle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            * {
                margin: 0;
            }
            
            .content {
                display: flex;
                justify-content: center;
            }
            
            .footer-block {
                position: absolute;
                bottom: 1rem;
            }
        </style>
    </head>
    <body>
        <div class="content">
            <h1>Hello World!</h1>
        </div>
        <!-- Footer -->
        <div class="footer-block">
            <!--
            利用 include 來加入 /WEB-INF/ContactSection.jsp
            /WEB-INF 是隱藏資料夾，會被瀏覽器擋起來
            /：斜線代表的是專案的根目錄
            -->
            <%@include file="/WEB-INF/ContactSection.jsp" %> 
            <!-- 會產生出：out.write("<p>Carrie | s1080321@mail.yzu.edu.tw</p>\n"); -->
        </div>
    </body>
</html>
